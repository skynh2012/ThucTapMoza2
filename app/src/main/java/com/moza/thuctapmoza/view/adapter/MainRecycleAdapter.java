package com.moza.thuctapmoza.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.MainList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * MainRecycleAdapter.java
 *
 * @author duyanh
 */

public class MainRecycleAdapter extends RecyclerView.Adapter<MainRecycleAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<MainList> mList;

    public MainRecycleAdapter(Context mContext, ArrayList<MainList> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MainList model = mList.get(position);
        if (model != null) {

            Picasso.with(mContext)
                    .load(model.getAvatar())
                    .into(holder.imvAvatar);

            holder.tvTitle.setText(model.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imvAvatar;
        public TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            imvAvatar = (CircleImageView) itemView.findViewById(R.id.imv_main_avata);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }
}
