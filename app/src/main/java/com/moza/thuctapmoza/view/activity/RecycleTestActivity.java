package com.moza.thuctapmoza.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.base.RecycleViewOnTouchListener;
import com.moza.thuctapmoza.config.Common;
import com.moza.thuctapmoza.model.MainList;
import com.moza.thuctapmoza.view.adapter.MainRecycleAdapter;

import java.util.ArrayList;

public class RecycleTestActivity extends AppCompatActivity {

    private RecyclerView rcvRecycleTest;
    private ArrayList<MainList> mList;
    private MainRecycleAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycle_test);

        rcvRecycleTest = (RecyclerView) findViewById(R.id.rcv_recycle_test);

        mList = new ArrayList<>();
        mAdapter = new MainRecycleAdapter(RecycleTestActivity.this, mList);
        rcvRecycleTest.setLayoutManager(new LinearLayoutManager(RecycleTestActivity.this, LinearLayoutManager.VERTICAL, false));
        rcvRecycleTest.setAdapter(mAdapter);
        rcvRecycleTest.addOnItemTouchListener(new RecycleViewOnTouchListener(RecycleTestActivity.this, new RecycleViewOnTouchListener.OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(RecycleTestActivity.this, LinearOneActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(RecycleTestActivity.this, LinearTwoActivity.class));
                        break;

                    case 2:
                        startActivity(new Intent(RecycleTestActivity.this, LinearThreeActivity.class));
                        break;

                    case 3:
                        startActivity(new Intent(RecycleTestActivity.this, GridOneActivity.class));
                        break;

                    case 4:
                        startActivity(new Intent(RecycleTestActivity.this, GridTwoActivity.class));
                        break;

                    case 5:
                        startActivity(new Intent(RecycleTestActivity.this, StaggOneActivity.class));
                        break;
                    case 6:
                        startActivity(new Intent(RecycleTestActivity.this, StaggTwoActivity.class));
                        break;
                    case 7:
                        startActivity(new Intent(RecycleTestActivity.this, GridOtherActivity.class));
                        break;
                }
            }
        }));
        faceData();
    }

    private void faceData() {
        MainList list;

        list = new MainList(Common.URL_AVATAR_1, "LinearLayout example 1");
        mList.add(list);
        list = new MainList(Common.URL_AVATAR_1, "LinearLayout example 2");
        mList.add(list);
        list = new MainList(Common.URL_AVATAR_1, "LinearLayout example 3");
        mList.add(list);
        list = new MainList(Common.URL_AVATAR_1, "StaggeredGridLayoutManager example 1");
        mList.add(list);
        list = new MainList(Common.URL_AVATAR_1, "StaggeredGridLayoutManager example 2");
        mList.add(list);
        list = new MainList(Common.URL_AVATAR_1, "GridLayoutManager example 1");
        mList.add(list);
        list = new MainList(Common.URL_AVATAR_1, "GridLayoutManager example 2");
        mList.add(list);
        list = new MainList(Common.URL_AVATAR_1, "Other item example");
        mList.add(list);
        mAdapter.notifyDataSetChanged();
    }
}
