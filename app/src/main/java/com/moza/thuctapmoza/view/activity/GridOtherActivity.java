package com.moza.thuctapmoza.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.config.Common;
import com.moza.thuctapmoza.model.GridOther;
import com.moza.thuctapmoza.view.adapter.GridOtherAdapter;

import java.util.ArrayList;

public class GridOtherActivity extends AppCompatActivity {
    private RecyclerView rcvGridOther;
    private ArrayList<GridOther> mListOther;
    private GridOtherAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_other);

        rcvGridOther = (RecyclerView) findViewById(R.id.rcv_grid_other);

        mListOther = new ArrayList<>();
        mAdapter = new GridOtherAdapter(GridOtherActivity.this, mListOther);

        rcvGridOther.setLayoutManager(new GridLayoutManager(GridOtherActivity.this, 2, LinearLayoutManager.VERTICAL, false));
        rcvGridOther.setAdapter(mAdapter);
        faceData();
    }

    private void faceData() {
        GridOther other;

        other = new GridOther();
        other.setPhoto("http://www.telegraph.co.uk/content/dam/Food%20and%20drink/2015-09/25sept/green-tea-alamy-large.jpg");
        other.setTopic("Drink Tea");
        other.setTopicColor(0);
        other.setType(Common.GRID_OTHER_TYPE_ONE);
        mListOther.add(other);

        other = new GridOther();
        other.setTime("20:20");
        other.setTitle("Hàng bao xinh bao chất đẹp nhé mẹ. không xênh không lấy tiền è.");
        other.setType(Common.GRID_OTHER_TYPE_TWO);
        mListOther.add(other);

        other = new GridOther();
        other.setTime("20:20");
        other.setTitle("Hàng bao xinh bao chất đẹp nhé mẹ. không xênh không lấy tiền è.");
        other.setType(Common.GRID_OTHER_TYPE_TWO);
        mListOther.add(other);

        other = new GridOther();
        other.setPhoto("http://www.telegraph.co.uk/content/dam/Food%20and%20drink/2015-09/25sept/green-tea-alamy-large.jpg");
        other.setTopic("Drink Tea");
        other.setTopicColor(2);
        other.setType(Common.GRID_OTHER_TYPE_ONE);
        mListOther.add(other);


        other = new GridOther();
        other.setPhoto("http://www.telegraph.co.uk/content/dam/Food%20and%20drink/2015-09/25sept/green-tea-alamy-large.jpg");
        other.setTopic("Drink Tea");
        other.setTopicColor(1);
        other.setType(Common.GRID_OTHER_TYPE_ONE);
        mListOther.add(other);

        other = new GridOther();
        other.setTime("20:20");
        other.setTitle("Hàng bao xinh bao chất đẹp nhé mẹ. không xênh không lấy tiền è.");
        other.setType(Common.GRID_OTHER_TYPE_TWO);
        mListOther.add(other);

        other = new GridOther();
        other.setTime("20:20");
        other.setTitle("Hàng bao xinh bao chất đẹp nhé mẹ. không xênh không lấy tiền è.");
        other.setType(Common.GRID_OTHER_TYPE_TWO);
        mListOther.add(other);

        other = new GridOther();
        other.setPhoto("http://www.telegraph.co.uk/content/dam/Food%20and%20drink/2015-09/25sept/green-tea-alamy-large.jpg");
        other.setTopic("Drink Tea");
        other.setTopicColor(3);
        other.setType(Common.GRID_OTHER_TYPE_ONE);
        mListOther.add(other);
        other = new GridOther();
        other.setPhoto("http://www.telegraph.co.uk/content/dam/Food%20and%20drink/2015-09/25sept/green-tea-alamy-large.jpg");
        other.setTopic("Drink Tea");
        other.setTopicColor(3);
        other.setType(Common.GRID_OTHER_TYPE_ONE);
        mListOther.add(other);

        other = new GridOther();
        other.setTime("20:20");
        other.setTitle("Hàng bao xinh bao chất đẹp nhé mẹ. không xênh không lấy tiền è.");
        other.setType(Common.GRID_OTHER_TYPE_TWO);
        mListOther.add(other);

        other = new GridOther();
        other.setTime("20:20");
        other.setTitle("Hàng bao xinh bao chất đẹp nhé mẹ. không xênh không lấy tiền è.");
        other.setType(Common.GRID_OTHER_TYPE_TWO);
        mListOther.add(other);

        other = new GridOther();
        other.setPhoto("http://www.telegraph.co.uk/content/dam/Food%20and%20drink/2015-09/25sept/green-tea-alamy-large.jpg");
        other.setTopic("Drink Tea");
        other.setTopicColor(3);
        other.setType(Common.GRID_OTHER_TYPE_ONE);
        mListOther.add(other);


        other = new GridOther();
        other.setPhoto("http://www.telegraph.co.uk/content/dam/Food%20and%20drink/2015-09/25sept/green-tea-alamy-large.jpg");
        other.setTopic("Drink Tea");
        other.setTopicColor(3);
        other.setType(Common.GRID_OTHER_TYPE_ONE);
        mListOther.add(other);

        other = new GridOther();
        other.setTime("20:20");
        other.setTitle("Hàng bao xinh bao chất đẹp nhé mẹ. không xênh không lấy tiền è.");
        other.setType(Common.GRID_OTHER_TYPE_TWO);
        mListOther.add(other);

        other = new GridOther();
        other.setTime("20:20");
        other.setTitle("Hàng bao xinh bao chất đẹp nhé mẹ. không xênh không lấy tiền è.");
        other.setType(Common.GRID_OTHER_TYPE_TWO);
        mListOther.add(other);

        other = new GridOther();
        other.setPhoto("http://www.telegraph.co.uk/content/dam/Food%20and%20drink/2015-09/25sept/green-tea-alamy-large.jpg");
        other.setTopic("Drink Tea");
        other.setTopicColor(3);
        other.setType(Common.GRID_OTHER_TYPE_ONE);
        mListOther.add(other);


        mAdapter.notifyDataSetChanged();
    }
}
