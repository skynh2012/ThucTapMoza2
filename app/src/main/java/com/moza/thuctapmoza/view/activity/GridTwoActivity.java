package com.moza.thuctapmoza.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.GridTwo;
import com.moza.thuctapmoza.util.ActionBarUtil;
import com.moza.thuctapmoza.view.adapter.GridTwoAdapter;

import java.util.ArrayList;

public class GridTwoActivity extends AppCompatActivity {
    private RecyclerView rcvGridTwo;
    private ArrayList<GridTwo> mListTwo;
    private GridTwoAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_two);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBarUtil.setTitleCenterActionBar("Favoite", getSupportActionBar());

        rcvGridTwo = (RecyclerView) findViewById(R.id.rcv_grid_two);

        mListTwo = new ArrayList<>();

        mAdapter = new GridTwoAdapter(GridTwoActivity.this, mListTwo);
        rcvGridTwo.setLayoutManager(new GridLayoutManager(GridTwoActivity.this, 2, LinearLayoutManager.VERTICAL, false));
        rcvGridTwo.setAdapter(mAdapter);
        faceData();
    }

    private void faceData() {
        GridTwo two;

        two = new GridTwo();
        two.setCover("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAvatar("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAge(23);
        two.setName("Khắc Hưng");
        two.setSex(true);

        mListTwo.add(two);

        two = new GridTwo();
        two.setCover("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/e/0/e0a93e9df0a781ff081d1414bdffc919_1487561175.jpg");
        two.setAvatar("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/e/0/e0a93e9df0a781ff081d1414bdffc919_1487561175.jpg");
        two.setAge(24);
        two.setName("Tóc Tiên");
        two.setSex(false);

        mListTwo.add(two);

        two = new GridTwo();
        two.setCover("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/e/c/ec280c3c5cb0156901950902d20f222b_1494941286.jpg");
        two.setAvatar("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/e/c/ec280c3c5cb0156901950902d20f222b_1494941286.jpg");
        two.setAge(25);
        two.setName("Hoàng Dương");
        two.setSex(true);

        mListTwo.add(two);

        two = new GridTwo();
        two.setCover("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/5/7544520bda4b442bfe11aa4691aed534_1496721494.jpg");
        two.setAvatar("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/5/7544520bda4b442bfe11aa4691aed534_1496721494.jpg");
        two.setAge(27);
        two.setName("Hiền Hồ");
        two.setSex(false);

        mListTwo.add(two);

        two = new GridTwo();
        two.setCover("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAvatar("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAge(23);
        two.setName("Khắc Hưng");
        two.setSex(true);

        mListTwo.add(two);

        two = new GridTwo();
        two.setCover("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAvatar("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAge(23);
        two.setName("Khắc Hưng");
        two.setSex(false);

        mListTwo.add(two);

        two = new GridTwo();
        two.setCover("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAvatar("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAge(23);
        two.setName("Khắc Hưng");
        two.setSex(false);

        mListTwo.add(two);

        two = new GridTwo();
        two.setCover("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAvatar("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAge(23);
        two.setName("Khắc Hưng");
        two.setSex(false);

        mListTwo.add(two);

        two = new GridTwo();
        two.setCover("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAvatar("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAge(23);
        two.setName("Khắc Hưng");
        two.setSex(false);

        mListTwo.add(two);

        two = new GridTwo();
        two.setCover("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAvatar("http://zmp3-photo-td.zadn.vn/thumb/165_165/avatars/7/b/7b7b6edb8801aa6e8373b5e3eff04000_1495764389.jpg");
        two.setAge(23);
        two.setName("Khắc Hưng");
        two.setSex(false);

        mListTwo.add(two);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
