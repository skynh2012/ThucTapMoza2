package com.moza.thuctapmoza.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.XmlModel;

import java.util.ArrayList;


/**
 * Created by Admin on 17/7/2017.
 */

public class ReadXmlAdapter extends ArrayAdapter {
    private ArrayList<XmlModel> mArr;
    private LayoutInflater inflater;

    public ReadXmlAdapter(Context context, int resource, ArrayList<XmlModel> mArr) {
        super(context, resource, mArr);
        this.mArr = mArr;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_read_xml_test, parent, false);
            holder = new ViewHolder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_xml_title);
            holder.tvDescription = (TextView) convertView.findViewById(R.id.tv_xml_description);
            holder.tvLink = (TextView) convertView.findViewById(R.id.tv_xml_link);
            holder.tvGuid = (TextView) convertView.findViewById(R.id.tv_xml_guid);
            holder.tvPubDate = (TextView) convertView.findViewById(R.id.tv_xml_pubdate);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        XmlModel model = mArr.get(position);
        if (model != null) {
            holder.tvTitle.setText(model.getTitle());
            holder.tvDescription.setText(model.getDescription());
            holder.tvLink.setText(model.getLink());
            holder.tvGuid.setText(model.getGuid());
            holder.tvPubDate.setText(model.getPubDate());
        }
        return convertView;
    }

    public class ViewHolder {
        public TextView tvTitle, tvDescription, tvLink, tvPubDate, tvGuid;
    }
}
