package com.moza.thuctapmoza.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.VolleyError;
import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.XmlModel;
import com.moza.thuctapmoza.modelmanager.ModelManager;
import com.moza.thuctapmoza.modelmanager.ModelManagerListener;
import com.moza.thuctapmoza.modelmanager.RssParseUtility;

import java.util.ArrayList;

public class RssActivity extends AppCompatActivity {

    private static final String urlRss = "http://vnexpress.net/rss/tin-moi-nhat.rss";
    ListView lsvRss;
    ArrayList<String> mArr;
    ArrayAdapter<String> mAdapter;
    ArrayList<XmlModel> mArrModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss);
        initView();
        loadData();
    }

    private void initView() {
        lsvRss = (ListView) findViewById(R.id.lsv_rss);
        mArr = new ArrayList<>();
        mAdapter = new ArrayAdapter<String>(RssActivity.this, android.R.layout.simple_list_item_1, mArr);
        lsvRss.setAdapter(mAdapter);

        lsvRss.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long which) {
                XmlModel model = mArrModel.get(position);

                Intent intent = new Intent(RssActivity.this, WebviewActivity.class);
                intent.putExtra("link", model.getLink());
                startActivity(intent);
            }
        });
    }

    private void loadData() {
        ModelManager.getRss(RssActivity.this, urlRss, false, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {

            }

            @Override
            public void onSuccess(Object object) {
                RssParseUtility parseUtility = new RssParseUtility(object.toString());

                mArrModel = parseUtility.getDataList();

                for (XmlModel model : mArrModel) {
                    mArr.add(model.getTitle());
                }
                mAdapter.notifyDataSetChanged();
            }
        });
    }
}
