package com.moza.thuctapmoza.view.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.StaggTwo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 22/06/2017.
 */

public class StaggTwoAdapter extends RecyclerView.Adapter<StaggTwoAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<StaggTwo> mList;

    public StaggTwoAdapter(Context mContext, ArrayList<StaggTwo> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stagg_two, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        StaggTwo staggTwo = mList.get(position);
        if (staggTwo != null) {
            if (!staggTwo.getPhoto().equals("")) {
                Picasso.with(mContext)
                        .load(staggTwo.getPhoto())
                        .into(holder.imvPhoto);
            }
            holder.tvName.setText(staggTwo.getName());
            holder.tvTitle.setText(staggTwo.getTitle());
            holder.tvLike.setText(String.valueOf(staggTwo.getLike()));
            holder.tvTime.setText(staggTwo.getTime());
            holder.btnTopic.setText(staggTwo.getTopic());
            switch (staggTwo.getTopicColor()) {
                case 0:
                    holder.btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_blue));
                    break;

                case 1:
                    holder.btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_yellow));
                    break;

                case 2:
                    holder.btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_green));
                    break;

                case 3:
                    holder.btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_green_yellow));
                    break;

                case 4:
                    holder.btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_red));
                    break;

                case 5:
                    holder.btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_blue_violet));
                    break;
            }

            if (staggTwo.isLiked()) {
                holder.imvLike.setImageResource(R.drawable.ic_heart);
            } else {
                holder.imvLike.setImageResource(R.drawable.ic_like);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imvPhoto, imvLike;
        public TextView tvName, tvTitle, tvLike, tvTime, btnTopic;

        public ViewHolder(View itemView) {
            super(itemView);
            imvPhoto = (ImageView) itemView.findViewById(R.id.imv_stagg_two_photo);
            imvLike = (ImageView) itemView.findViewById(R.id.imv_stagg_two_like);
            btnTopic = (TextView) itemView.findViewById(R.id.btn_stagg_two_topic);
            tvName = (TextView) itemView.findViewById(R.id.tv_stagg_two_name);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_stagg_two_title);
            tvLike = (TextView) itemView.findViewById(R.id.tv_stagg_two_like);
            tvTime = (TextView) itemView.findViewById(R.id.tv_stagg_two_time);
        }
    }
}
