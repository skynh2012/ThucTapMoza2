package com.moza.thuctapmoza.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.GridOne;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 21/06/2017.
 */

public class GridOneAdapter extends RecyclerView.Adapter<GridOneAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<GridOne> mList;

    public GridOneAdapter(Context mContext, ArrayList<GridOne> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_one, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GridOne gridOne = mList.get(position);
        if (gridOne != null) {
            if (!gridOne.getCover().equals("")) {
                Picasso.with(mContext)
                        .load(gridOne.getCover())
                        .into(holder.imvCover);
            }
            if (!gridOne.getAvatar().equals("")) {
                Picasso.with(mContext)
                        .load(gridOne.getAvatar())
                        .into(holder.imvAvatar);
            }

            holder.tvTitle.setText(gridOne.getTitle());
            holder.tvLike.setText(String.valueOf(gridOne.getLike()));
            holder.tvComment.setText(String.valueOf(gridOne.getComment()));
        }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imvCover;
        public CircleImageView imvAvatar;
        public TextView tvTitle, tvLike, tvComment;

        public ViewHolder(View itemView) {
            super(itemView);

            imvCover = (ImageView) itemView.findViewById(R.id.imv_grid_one_cover);
            imvAvatar = (CircleImageView) itemView.findViewById(R.id.imv_grid_one_avatar);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_grid_one_tittle);
            tvLike = (TextView) itemView.findViewById(R.id.tv_grid_one_like);
            tvComment = (TextView) itemView.findViewById(R.id.tv_grid_one_comment);
        }
    }
}
