package com.moza.thuctapmoza.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.MenuItem;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.StaggOne;
import com.moza.thuctapmoza.util.ActionBarUtil;
import com.moza.thuctapmoza.view.adapter.StaggOneAdapter;

import java.util.ArrayList;

public class StaggOneActivity extends AppCompatActivity {

    private RecyclerView rcvStaggOne;
    private ArrayList<StaggOne> mListOne;
    private StaggOneAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stagg_one);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBarUtil.setTitleCenterActionBar("Stagg one", getSupportActionBar());

        rcvStaggOne = (RecyclerView) findViewById(R.id.rcv_stagg_one);

        mListOne = new ArrayList<>();

        mAdapter = new StaggOneAdapter(StaggOneActivity.this, mListOne);

        rcvStaggOne.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        rcvStaggOne.setAdapter(mAdapter);
        faceData();
    }

    private void faceData() {
        StaggOne staggOne;

        staggOne = new StaggOne("http://www.flyingflowers.co.uk/ff_images/Row1_ElegantRoseFreesia-1.jpg?&unsharpmask-radius-amount=2&unsharpmask-value-amount=2", "Flowers");
        mListOne.add(staggOne);

        staggOne = new StaggOne("https://cdn.pixabay.com/photo/2016/07/04/18/16/sun-flower-1497092_960_720.jpg", "Beautiful");
        mListOne.add(staggOne);

        staggOne = new StaggOne("http://static.asiawebdirect.com/m/phuket/portals/phuket-com/homepage/island/naiharn-beach/pagePropertiesImage/naiharn-beach.jpg", "Beach");
        mListOne.add(staggOne);

        staggOne = new StaggOne("https://s-media-cache-ak0.pinimg.com/originals/45/76/42/457642cdb22f479f988095cceaf7fa09.jpg", "Green");
        mListOne.add(staggOne);

        staggOne = new StaggOne("https://cdn.pixabay.com/photo/2013/02/20/10/32/painting-83668_640.jpg", "Red");
        mListOne.add(staggOne);

        staggOne = new StaggOne("http://www.cats.org.uk/uploads/images/featurebox_sidebar_kids/grief-and-loss.jpg", "Cat");
        mListOne.add(staggOne);

        staggOne = new StaggOne("http://www.flyingflowers.co.uk/ff_images/Row1_ElegantRoseFreesia-1.jpg?&unsharpmask-radius-amount=2&unsharpmask-value-amount=2", "Flowers");
        mListOne.add(staggOne);

        staggOne = new StaggOne("http://az616578.vo.msecnd.net/files/2016/06/16/636017001832422628-355544219_beach%20pic.jpg", "Beach");
        mListOne.add(staggOne);

        staggOne = new StaggOne("http://www.flyingflowers.co.uk/ff_images/Row1_ElegantRoseFreesia-1.jpg?&unsharpmask-radius-amount=2&unsharpmask-value-amount=2", "Flowers");
        mListOne.add(staggOne);

        staggOne = new StaggOne("https://static.pexels.com/photos/87452/flowers-background-butterflies-beautiful-87452.jpeg", "Flowers");
        mListOne.add(staggOne);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
