package com.moza.thuctapmoza.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.XmlModel;
import com.moza.thuctapmoza.modelmanager.RssParseUtility;
import com.moza.thuctapmoza.view.adapter.ReadXmlAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class XmlActivity extends AppCompatActivity {
    private ArrayList<XmlModel> mArr;
    private ReadXmlAdapter mAdapter;
    ListView lsvXmlTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xml);

        initView();
        loadData();

    }

    private void initView() {
        lsvXmlTest = (ListView) findViewById(R.id.lsv_xml_test);
        mArr = new ArrayList<>();
        mAdapter = new ReadXmlAdapter(XmlActivity.this, R.layout.item_read_xml_test, mArr);
        lsvXmlTest.setAdapter(mAdapter);
    }

    private String loadXmlFromAssets() {
        String xml = null;

        try {
            InputStream stream = this.getAssets().open("xmltest.xml");
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            xml = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return xml;
    }

    private void loadData() {
        String xml = loadXmlFromAssets();

        RssParseUtility parseUtility = new RssParseUtility(xml);

        ArrayList<XmlModel> lstModel = parseUtility.getDataList();

        for (XmlModel model : lstModel) {
            mArr.add(model);
        }
        mAdapter.notifyDataSetChanged();
    }
}
