package com.moza.thuctapmoza.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.LinearTwo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 21/06/2017.
 */

public class LinearTwoAdapter extends RecyclerView.Adapter<LinearTwoAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<LinearTwo> mList;

    public LinearTwoAdapter(Context mContext, ArrayList<LinearTwo> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_linear_two, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LinearTwo two = mList.get(position);

        if (two != null) {
            if (!two.getAvata().equals("")) {
                Picasso.with(mContext)
                        .load(two.getAvata())
                        .into(holder.imvAvatar);
            }
            holder.tvName.setText(two.getName());
            holder.tvTime.setText(two.getTime());
            holder.tvMessage.setText(two.getMessage());
            if (!two.getImage().equals("")) {
                Picasso.with(mContext)
                        .load(two.getImage())
                        .into(holder.imvImage);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imvAvatar;
        public ImageView imvImage;
        public TextView tvName, tvTime, tvMessage;

        public ViewHolder(View itemView) {
            super(itemView);
            imvAvatar = (CircleImageView) itemView.findViewById(R.id.imv_linear_two_avatar);
            imvImage = (ImageView) itemView.findViewById(R.id.imv_linear_two_image);
            tvName = (TextView) itemView.findViewById(R.id.tv_linear_two_name);
            tvTime = (TextView) itemView.findViewById(R.id.tv_linear_two_time);
            tvMessage = (TextView) itemView.findViewById(R.id.tv_linear_two_message);
        }
    }
}
