package com.moza.thuctapmoza.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.LinearThree;

import java.util.ArrayList;

/**
 * Created by Administrator on 21/06/2017.
 */

public class LinearThreeAdapter extends RecyclerView.Adapter<LinearThreeAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<LinearThree> mList;

    public LinearThreeAdapter(Context mContext, ArrayList<LinearThree> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_linear_three, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LinearThree three = mList.get(position);
        if (three != null) {
            holder.rtbRate.setRating(three.getRate());
            holder.tvAvatar.setText(String.format("%.1f", three.getRate()));
            holder.tvName.setText(three.getName());
            holder.tvDate.setText(three.getDate());
            holder.tvTitle.setText(three.getTitle());
            holder.tvContent.setText(three.getContent());
        }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvAvatar, tvDate, tvName, tvTitle, tvContent;
        public RatingBar rtbRate;

        public ViewHolder(View itemView) {
            super(itemView);
            tvAvatar = (TextView) itemView.findViewById(R.id.tv_linear_three_avatar);
            tvDate = (TextView) itemView.findViewById(R.id.tv_linear_three_date);
            tvName = (TextView) itemView.findViewById(R.id.tv_linear_three_name);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_linear_three_title);
            tvContent = (TextView) itemView.findViewById(R.id.tv_linear_three_content);
            rtbRate = (RatingBar) itemView.findViewById(R.id.rtb_linear_three_rate);
        }
    }
}
