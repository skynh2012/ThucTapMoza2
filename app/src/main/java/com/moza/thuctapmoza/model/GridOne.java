package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 21/06/2017.
 */

public class GridOne {
    private String cover;
    private String avatar;
    private String title;
    private int like;
    private int comment;

    public GridOne() {
    }

    public GridOne(String cover, String avatar, String title, int like, int comment) {
        this.cover = cover;
        this.avatar = avatar;
        this.title = title;
        this.like = like;
        this.comment = comment;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getComment() {
        return comment;
    }

    public void setComment(int comment) {
        this.comment = comment;
    }
}
