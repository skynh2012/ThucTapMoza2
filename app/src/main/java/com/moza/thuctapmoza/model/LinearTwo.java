package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 21/06/2017.
 */

public class LinearTwo {
    private String avata;
    private String name;
    private String time;
    private String message;
    private String image;

    public LinearTwo() {
    }

    public LinearTwo(String avata, String name, String time, String message, String image) {
        this.avata = avata;
        this.name = name;
        this.time = time;
        this.message = message;
        this.image = image;
    }

    public String getAvata() {
        return avata;
    }

    public void setAvata(String avata) {
        this.avata = avata;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
