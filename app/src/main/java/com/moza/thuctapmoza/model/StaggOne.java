package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 22/06/2017.
 */

public class StaggOne {
    private String photo;
    private String title;

    public StaggOne() {
    }

    public StaggOne(String photo, String title) {
        this.photo = photo;
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
