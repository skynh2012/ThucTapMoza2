package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 22/06/2017.
 */

public class StaggTwo {
    private String photo;
    private String topic;
    private int topicColor;
    private String name;
    private String title;
    private int like;
    private boolean isLiked;
    private String time;

    public StaggTwo() {
    }

    public StaggTwo(String photo, String topic, int topicColor, String name, String title, int like, String time, boolean isLiked) {
        this.photo = photo;
        this.topic = topic;
        this.topicColor = topicColor;
        this.name = name;
        this.title = title;
        this.like = like;
        this.time = time;
        this.isLiked = isLiked;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getTopicColor() {
        return topicColor;
    }

    public void setTopicColor(int topicColor) {
        this.topicColor = topicColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
