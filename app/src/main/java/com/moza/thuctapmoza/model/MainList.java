package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 20/06/2017.
 */

public class MainList {
    private String avatar;
    private String title;

    public MainList() {
    }

    public MainList(String avatar, String title) {
        this.avatar = avatar;
        this.title = title;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
