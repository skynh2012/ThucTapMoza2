package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 28/06/2017.
 */

public class User {
    private String FullName;
    private String Email;

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
