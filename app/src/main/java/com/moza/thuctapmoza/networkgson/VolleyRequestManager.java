package com.moza.thuctapmoza.networkgson;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Administrator on 03/07/2017.
 */

public class VolleyRequestManager {
    private static RequestQueue mRequestQueue;

    private VolleyRequestManager() {
    }

    /**
     * @param context
     * 			application context
     */
    public static void init(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
    }

    /**
     * @return
     * 		instance of the queue
     * @throws
     * 	 if initialize has not yet been called
     */
    public static RequestQueue getRequestQueue() {
        if (mRequestQueue != null) {
            return mRequestQueue;
        } else {
            throw new IllegalStateException("Not initialized");
        }
    }
}
