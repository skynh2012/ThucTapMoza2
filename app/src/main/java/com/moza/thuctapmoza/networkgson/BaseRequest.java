package com.moza.thuctapmoza.networkgson;

import android.content.Context;

import com.moza.thuctapmoza.R;

import java.util.HashMap;

/**
 * Created by Administrator on 03/07/2017.
 */

public class BaseRequest {
    private static BaseRequest instance;
    private static MyProgressDialog myProgressDialog;

    /**
     * init this
     */
    public static void init() {
        instance = new BaseRequest();
        ApiManager.getInstance();
    }

    /**
     * get instance
     */
    public static BaseRequest getInstance() {
        if (instance != null) {
            return instance;
        } else {
            throw new IllegalStateException("Not initialized");
        }
    }

    /**
     * show hide progress bar
     */
    private void showProgress(boolean isOpen) {
        if (isOpen) {
            myProgressDialog.show();
        }
    }

    public void hideProgress(boolean isOpen) {
        if (isOpen) {
            myProgressDialog.dismiss();
        }
    }

    // caching is false. ProgressBar is option
    public static void get(Context context, final String url, final HashMap<String, String> params,
                           boolean isShowProgress, final ApiManager.CompleteListener completeListener) {
        getInstance().getRequest(context, url, params, isShowProgress, completeListener);
    }

    // default is showing progress and not caching
    public static void get(Context context, final String url, final HashMap<String, String> params,
                           final ApiManager.CompleteListener completeListener) {
        getInstance().getRequest(context, url, params, true, completeListener);
    }

    /**
     * Using method get.
     *
     * @param url
     * @param params
     * @param isShowProgress   showing progress or not?
     * @param completeListener listener
     */
    private void getRequest(Context context, final String url, final HashMap<String, String> params,
                            final boolean isShowProgress, final ApiManager.CompleteListener completeListener) {
        if (NetworkUtility.isNetworkAvailable()) {
            myProgressDialog = new MyProgressDialog(context);
            showProgress(isShowProgress);
            ApiManager.post(url, params, new ApiManager.CompleteListener() {

                @Override
                public void onSuccess(ApiResponse response) {
                    hideProgress(isShowProgress);
                    if (response.getDataObject() != null || response.getDataArray() != null) {
                        completeListener.onSuccess(response);
                    } else if (!response.isError()) {
                        completeListener.onSuccess(response);
                    }
                }

                @Override
                public void onError(String message) {
                    hideProgress(isShowProgress);
                    completeListener.onError(message);
                }
            });
        } else {
            //AppUtil.showToast(context, R.string.msg_connection_network_error);
        }
    }
}
