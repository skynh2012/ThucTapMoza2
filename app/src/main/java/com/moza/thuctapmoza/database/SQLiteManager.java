package com.moza.thuctapmoza.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.moza.thuctapmoza.model.PhotoInfo;

import java.util.ArrayList;

/**
 * Created by Administrator on 04/07/2017.
 */

public class SQLiteManager extends SQLiteOpenHelper {
    public static final int DATA_VERSION = 1;
    public static final String DATA_NAME = "Test";
    public static final String TABLE_NAME = "tbTest";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DOWNLOAD = "download";
    public static final String COLUMN_VIEW = "view";
    public static final String COLUMN_IMAGE = "images";
    public static final String COLUMN_DESCRIPTION = "description";

    public static final String CreateTable = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMN_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME +
            " VARCHAR, " + COLUMN_DOWNLOAD +
            " INTEGER, " + COLUMN_VIEW +
            " INTEGER, " + COLUMN_IMAGE +
            " VARCHAR, " + COLUMN_DESCRIPTION +
            " VARCHAR);";

    public SQLiteManager(Context context) {
        super(context, DATA_NAME, null, DATA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CreateTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int curentVerion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        onCreate(sqLiteDatabase);
    }

    public void addPhoto(PhotoInfo info) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (!isExists(info)) {
            ContentValues values = new ContentValues();

            values.put(COLUMN_ID, info.getId());
            values.put(COLUMN_NAME, info.getName());
            values.put(COLUMN_DOWNLOAD, info.getDownload());
            values.put(COLUMN_VIEW, info.getView());
            values.put(COLUMN_IMAGE, info.getImages());
            values.put(COLUMN_DESCRIPTION, info.getDescription());

            db.insert(TABLE_NAME, null, values);
            db.close();
        }
    }

    public PhotoInfo getPhoto(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME,
                new String[]{COLUMN_ID, COLUMN_NAME, COLUMN_DOWNLOAD, COLUMN_VIEW, COLUMN_IMAGE, COLUMN_DESCRIPTION},
                COLUMN_ID + "=" + id, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }

        PhotoInfo info = new PhotoInfo();
        info.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
        info.setName(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
        info.setDownload(cursor.getInt(cursor.getColumnIndex(COLUMN_DOWNLOAD)));
        info.setView(cursor.getInt(cursor.getColumnIndex(COLUMN_VIEW)));
        info.setImages(cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE)));
        info.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));

        return info;
    }

    public ArrayList<PhotoInfo> getAllPhoto() {
        ArrayList<PhotoInfo> lstInfo = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                PhotoInfo info = new PhotoInfo();
                info.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                info.setName(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
                info.setDownload(cursor.getInt(cursor.getColumnIndex(COLUMN_DOWNLOAD)));
                info.setView(cursor.getInt(cursor.getColumnIndex(COLUMN_VIEW)));
                info.setImages(cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE)));
                info.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));

                lstInfo.add(info);
            } while (cursor.moveToNext());
        }

        return lstInfo;
    }

    public int getPhotoCount() {
        String query = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        return cursor.getCount();
    }

    public void updatePhoto(PhotoInfo info) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_NAME, info.getName());
        values.put(COLUMN_DOWNLOAD, info.getDownload());
        values.put(COLUMN_VIEW, info.getView());
        values.put(COLUMN_IMAGE, info.getImages());
        values.put(COLUMN_DESCRIPTION, info.getDescription());

        db.update(TABLE_NAME, values, COLUMN_ID + "=" + info.getId(), null);
        db.close();
    }

    public void deletePhoto(PhotoInfo info) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + "=" + info.getId(), null);
        db.close();
    }

    public boolean isExists(PhotoInfo info) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME,
                new String[]{COLUMN_ID, COLUMN_NAME, COLUMN_DOWNLOAD, COLUMN_VIEW, COLUMN_IMAGE, COLUMN_DESCRIPTION},
                COLUMN_ID + "=" + info.getId(), null, null, null, null);
        return cursor.getCount() > 0 ? true : false;
    }

    public int getNextId() {
        int nextId = 0;
        String query = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
                if (id > nextId) {
                    nextId = id;
                }
            } while (cursor.moveToNext());
        }
        return nextId + 1;
    }
}
