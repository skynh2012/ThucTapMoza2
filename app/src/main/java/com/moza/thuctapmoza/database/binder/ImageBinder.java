package com.moza.thuctapmoza.database.binder;

import android.database.sqlite.SQLiteStatement;

import com.moza.thuctapmoza.database.ParameterBinder;
import com.moza.thuctapmoza.model.PhotoInfo;


public class ImageBinder implements ParameterBinder {
    @Override
    public void bind(SQLiteStatement statement, Object object) {
        PhotoInfo info = (PhotoInfo) object;
        statement.bindLong(1, info.getId());
        statement.bindString(2, info.getName());
        statement.bindString(3, info.getImages());
        statement.bindString(4, info.getDescription());
        statement.bindLong(5, info.getDownload());
        statement.bindLong(6, info.getView());
    }
}
