package com.moza.thuctapmoza.network;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.moza.thuctapmoza.R;

/**
 * Created by Administrator on 28/06/2017.
 */

public class MyProgressDialog extends Dialog {

    public MyProgressDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(
                R.drawable.bg_black_transparent);
        setContentView(R.layout.layout_progress_dialog);

    }
}

