package com.moza.thuctapmoza.config;

import android.annotation.SuppressLint;

import com.moza.thuctapmoza.util.PacketUtility;

/**
 * Created by Administrator on 07/07/2017.
 */

public class DatabaseConfig {
    public final int DATA_VERSION = 2;
    public final String DATA_NAME = "Test";
    public static final String TABLE_NAME = "tbTest";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DOWNLOAD = "download";
    public static final String COLUMN_VIEW = "view";
    public static final String COLUMN_IMAGE = "images";
    public static final String COLUMN_DESCRIPTION = "description";

    public final String CreateTable = "CREATE TABLE " + TABLE_NAME
            + "(" + COLUMN_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME +
            " INTEGER, " + COLUMN_IMAGE +
            " VARCHAR, " + COLUMN_DESCRIPTION +
            " VARCHAR, " + COLUMN_DOWNLOAD +
            " INTEGER, " + COLUMN_VIEW +
            " VARCHAR);";

    /**
     * Get database version
     *
     * @return
     */
    public int getDatabaseVersion() {
        return DATA_VERSION;
    }

    /**
     * Get database name
     *
     * @return
     */
    public String getDatabaseName() {
        return DATA_NAME;
    }

    /**
     * Get database path
     *
     * @return
     */
    @SuppressLint("SdCardPath")
    public String getDatabasePath() {
        return "/data/data/" + PacketUtility.getPacketName() + "/databases/";
    }

    /**
     * Get database full path
     *
     * @return
     */
    public String getDatabaseFullPath() {
        return getDatabasePath() + DATA_NAME;
    }
}
