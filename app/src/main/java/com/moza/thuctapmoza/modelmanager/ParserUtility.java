package com.moza.thuctapmoza.modelmanager;

import com.moza.thuctapmoza.model.PhotoInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Administrator on 28/06/2017.
 */

public class ParserUtility {
    public static ArrayList<PhotoInfo> parsePhoto(String json) {
        ArrayList<PhotoInfo> list = new ArrayList<PhotoInfo>();
        try {
            JSONObject object = new JSONObject(json);
            if (getStringValue(object, "status").equalsIgnoreCase("SUCCESS")) {
                JSONArray array = object.getJSONObject("data").getJSONArray("galleries");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    PhotoInfo info = new PhotoInfo();
                    info.setId(obj.getInt("id"));
                    info.setName(obj.getString("name"));
                    info.setDescription(obj.getString("description"));
                    info.setDownload(obj.getInt("download"));
                    info.setView(obj.getInt("view"));
                    info.setImages(obj.getString("images"));

                    list.add(info);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private static String getStringValue(JSONObject obj, String key) {
        try {
            return obj.isNull(key) ? "" : obj.getString(key);
        } catch (JSONException e) {
            return "";
        }
    }
}
