package com.moza.thuctapmoza.modelmanager;

import com.android.volley.VolleyError;

public interface ModelManagerListener {

     void onError(VolleyError error);

     void onSuccess(Object object);
}

